(in-package :nea/ircd)

(defparameter *ircv3-server-time-format*
  `((:year 4) #\- (:month 2) #\- (:day 2) #\T (:hour 2) #\: (:min 2) #\: (:sec 2) #\. (:msec 3) #\Z)
  ;; <value> ::= YYYY-MM-DDThh:mm:ss.sssZ
  "IRCv3 server-time extension time format.")
(defparameter *human-time-format*
  '((:hour 2) #\: (:min 2) #\: (:sec 2) #\Z)
  "Human-friendly relative time format.")
(defparameter *human-date-and-time-format*
  '((:year 4) #\- (:month 2) #\- (:day 2) #\Space (:hour 2) #\: (:min 2) #\: (:sec 2) #\Z)
  "Human-friendly date and time format.")
(defparameter *significant-time-difference-seconds* 5
  "Number of seconds under which message timestamp differences are ignored when printing them to humans.")

(defun simple-date->local-time (ts)
  "Converts a SIMPLE-DATE timestamp to a LOCAL-TIME timestamp."
  (multiple-value-bind (year month day hour minute sec msec)
      (simple-date:decode-timestamp ts)
    (local-time:encode-timestamp (* msec (expt 10 6)) sec minute hour day month year)))

(defun timestamp-to-v3-server-time (ts)
  "Converts TS (a LOCAL-TIME timestamp) to an IRCv3 server-time string."
  (local-time:format-timestring nil ts :format *ircv3-server-time-format*))

(defun timestamp-to-human-time-prefix (ts)
  "Converts TS (a LOCAL-TIME timestamp) to a string representing that timestamp, relative to the current timestamp right now. Returns NIL if there's no significant time difference.
For example, a message sent an hour ago might only show the time, whereas a message sent yesterday would include the date component as well."
  (let ((now (local-time:clock-now t)))
    (local-time:format-timestring
     nil ts
     :format (if (and (eql (local-time:day-of ts) (local-time:day-of now))
                      (eql (local-time:timestamp-year ts) (local-time:timestamp-year now)))
                 (if (<= (- (local-time:timestamp-to-unix now)
                            (local-time:timestamp-to-unix ts))
                         *significant-time-difference-seconds*)
                     (return-from timestamp-to-human-time-prefix nil)
                     *human-time-format*)
                 *human-date-and-time-format*))))

(setf local-time:*default-timezone* local-time:+utc-zone+)
