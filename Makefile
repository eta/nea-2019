LISP ?= sbcl

all:
	$(LISP) \
		--eval '(ql:quickload :nea)' \
		--eval '(asdf:make :nea)' \
		--eval '(quit)'
