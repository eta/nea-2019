(defpackage :nea/paxos
  (:nicknames paxos)
  (:use :common-lisp))

(defpackage :nea/utils
  (:nicknames utils u)
  (:use common-lisp)
  (:export split-string-at
           split-string-at-first
           starts-with
           string-empty-p
           strip-ws
           join-by-spaces
           if-nil))

(defpackage :nea/irc
  (:nicknames irc i)
  (:use :cl
	:cl-ppcre
        :nea/utils)
  (:export :irc-message
           :irc-message-source
           :make-irc-message
           :make-irc-message-source
           :serialize-irc-message-source
           :serialize-tags-to-string
           :with-irc-source
           :to-irc-message-source
           :irc-message-serialize
           :irc-message-parse
           :read-irc-message
           :write-irc-message
           :invalid-arguments
           :invalid-command-name
           :parse-as-generic
           :parse-tags
	   :parse-sasl-payload
	   :attempted-command-name
           :message-tags
           :message-source
           :message-command
           :message-parameters
           :message-source-nick
           :message-source-user
           :message-source-host))

(defpackage :nea/ircd
  (:nicknames ircd)
  (:use :cl
        :nea/irc
        :nea/utils
        :usocket
	:cl-ppcre
   :bordeaux-threads)
  (:export :main))


