(in-package :nea/irc)

(defun parse-sasl-payload-inner (accum rest)
  (multiple-value-bind (part rest)
      (split-string-at-first rest #\Nul t)
    (if (string-empty-p rest)
        (reverse (cons part accum))
        (parse-sasl-payload-inner (cons part accum) rest))))

(defun parse-sasl-payload (payload)
  (let ((decoded-payload
          (babel:octets-to-string (qbase64:decode-string payload) :encoding :utf-8)))
    (parse-sasl-payload-inner nil decoded-payload)))
