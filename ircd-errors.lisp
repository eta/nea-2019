(in-package :nea/ircd)

(define-condition irc-user-error (error)
  ((numeric
    :initarg :numeric
    :reader irc-user-error-numeric)
   (args
    :initarg :args
    :reader irc-user-error-args
    :initform nil)
   (text
    :initarg :text
    :initform nil
    :reader irc-user-error-text))
  (:report
   (lambda (c s)
     (format s "~A"
             (or
              (irc-user-error-text c)
              (find-numeric-text (irc-user-error-numeric c))
              "Unknown error")))))


(defun raise-user-error (numeric &rest args)
  (error 'irc-user-error :numeric numeric :args args)
  nil)

(defun raise-user-error-with-text (numeric text &rest args)
  (error 'irc-user-error :numeric numeric :text text :args args)
  nil)
