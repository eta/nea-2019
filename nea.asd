(defsystem "nea"
  :depends-on ("usocket" "bordeaux-threads" "uiop" "cl-ppcre" "qbase64"
                         "babel" "postmodern" "ironclad" "cl-postgres-plus-uuid"
                         "simple-date" "simple-date/postgres-glue"
                         "local-time" "closer-mop" "unix-opts" "trivial-backtrace")
  :serial t
  :build-operation "program-op"
  :build-pathname "nea-ircd"
  :entry-point "nea/ircd:main"
  :components
  ((:file "packages")
   (:file "paxos")
   (:file "utils")
   (:file "irc")
   (:file "irc-messages")
   (:file "irc-parser")
   (:file "ircd-types")
   (:file "ircd-db")
   (:file "timestamps")
   (:file "ircd-users")
   (:file "ircd-messages")
   (:file "ircd-groupchats")
   (:file "sasl")
   (:file "ircd-errors")
   (:file "ircd-registration")
   (:file "ircd-config")
   (:file "ircd")))
