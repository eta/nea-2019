;;; Command line configuration

(in-package :nea/ircd)

(opts:define-opts
  (:name :help
   :description "Print this help text."
   :short #\h
   :long "help")
  (:name :server-name
   :description "Set the name of the local server."
   :short #\n
   :required t
   :meta-var "NAME"
   :arg-parser #'identity
   :long "server-name")
  (:name :listen-host
   :description "The hostname to listen on. Default: localhost"
   :short #\l
   :meta-var "HOST"
   :arg-parser #'identity
   :long "listen-host")
  (:name :listen-port
   :description "The port to listen on. Default: 6667"
   :short #\p
   :meta-var "PORT"
   :arg-parser #'parse-integer
   :long "listen-port")
  (:name :db-host
   :description "The hostname of the PostgreSQL database server. Default: localhost"
   :short #\H
   :meta-var "HOST"
   :arg-parser #'identity
   :long "db-host")
  (:name :db-port
   :description "The port of the PostgreSQL database server. Default: 5432"
   :short #\P
   :meta-var "PORT"
   :arg-parser #'parse-integer
   :long "db-port")
  (:name :db-username
   :description "The username to use for the PostgreSQL database server. Default: nea"
   :short #\U
   :meta-var "USERNAME"
   :arg-parser #'identity
   :long "db-username")
  (:name :db-password
   :description "The password to use for the PostgreSQL database server. Default: none"
   :short #\W
   :meta-var "USERNAME"
   :arg-parser #'identity
   :long "db-password")
  (:name :db-database
   :description "The database to access on the PostgreSQL database server. Default: nea"
   :short #\D
   :meta-var "USERNAME"
   :arg-parser #'identity
   :long "db-database")
  (:name :new-username
   :description "If this argument is supplied, creates a new user with this username."
   :meta-var "USERNAME"
   :arg-parser #'identity
   :long "new-username")
  (:name :new-password
   :description "If this argument is supplied with --new-username, sets the password of the new user."
   :meta-var "PASSWORD"
   :arg-parser #'identity
   :long "new-password"))
