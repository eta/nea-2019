;; Handling databasey whatnots

(in-package :nea/ircd)

(defparameter *local-server-id* 1
  "Server ID of the local server.")
(defparameter *default-database-args* '("nea" "nea" "" "localhost" :port 5432)
  "Default arguments to pass to POSTMODERN:CONNECT.")

(defmacro execute-one (&rest args)
  "Like POSTMODERN:EXECUTE, but raises an error if the amount of rows affected wasn't equal to 1 (and rolls back the transaction)"
  `(pomo:with-logical-transaction ()
     (let ((rows-modified (pomo:execute ,@args)))
       (assert (eql rows-modified 1) () "Modified ~A rows in an EXECUTE-ONE command" rows-modified))))

(cl-postgres-plus-uuid:set-uuid-sql-reader)

(defclass db-system-message ()
  ((msgid :col-type uuid
          :initarg :msgid
          :reader dbm-msgid)
   (seq :col-type int
        :col-default (:nextval messages_sequence)
        :initarg :seq
        :reader dbm-seq)
   (type :col-type int
         :initarg :type
         :reader dbm-type)
   (user-ref :col-type int
             :col-name user_ref
             :initarg :user-ref
             :reader dbm-user-ref)
   (target-user-ref :col-type int
                    :col-name target_user_ref
                    :initarg :target-user-ref
                    :reader dbm-target-user-ref)
   (groupchat-ref :col-type uuid
                  :col-name groupchat_ref
                  :initarg :groupchat-ref
                  :reader dbm-groupchat-ref)
   (body :col-type varchar
         :initarg :body
         :reader dbm-body)
   (local-ts :col-type timestamp
             :col-name local_ts
             :initarg :local-ts
             :initform (simple-date:universal-time-to-timestamp (get-universal-time))
             :reader dbm-local-ts))
  (:metaclass pomo:dao-class)
  (:table-name system_messages))

(defclass db-message ()
  ((msgid :col-type uuid
          :initarg :msgid
          :reader dbm-msgid)
   (seq :col-type int
        :col-default (:nextval messages_sequence)
        :initarg :seq
        :reader dbm-seq)
   (user-from :col-type int
              :col-name user_from
              :initarg :user-from
              :reader dbm-user-from)
   (user-to :col-type int
            :col-name user_to
            :initarg :user-to
            :reader dbm-user-to)
   (groupchat-to :col-type int
                 :col-name groupchat_to
                 :initarg :groupchat-to
                 :reader dbm-groupchat-to)
   (tags :col-type varchar
         :initarg :tags
         :reader dbm-tags)
   (body :col-type varchar
         :initarg :body
         :reader dbm-body)
   (origin-ts :col-type timestamp
              :col-name origin_ts
              :initarg :origin-ts
              :initform (simple-date:universal-time-to-timestamp (get-universal-time))
              :reader dbm-origin-ts)
   (local-ts :col-type timestamp
             :col-name local_ts
             :initarg :local-ts
             :initform (simple-date:universal-time-to-timestamp (get-universal-time))
             :reader dbm-local-ts)
   (is-notice :col-type boolean
              :col-name is_notice
              :initarg :is-notice
              :initform nil
              :reader dbm-is-notice))
  (:metaclass pomo:dao-class)
  (:table-name messages))

(defun initialize-database (&optional (args *default-database-args*))
  "Connect to the PostgreSQL database."
  (apply #'postmodern:connect-toplevel args))

(defun notify-new-messages ()
  "Notify the message delivery thread of new messages."
  (pomo:execute "NOTIFY \"ircd-messages\";"))
