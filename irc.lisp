(in-package :nea/irc)

(defclass irc-message ()
  ((tags
    :initarg :tags
    :initform nil
    :accessor message-tags)
   (source
    :initarg :source
    :initform nil
    :accessor message-source)
   (command
    :initarg :command
    :accessor message-command)
   (parameters
    :initarg :parameters
    :initform nil
    :accessor message-parameters)))

(defclass irc-message-source ()
  ((nick
    :initarg :nick
    :initform nil
    :accessor message-source-nick)
   (user
    :initarg :user
    :initform nil
    :accessor message-source-user)
   (host
    :initarg :host
    :accessor message-source-host)))

(defclass irc-message-notrailing (irc-message) ())

(defun with-irc-source (message source)
  (setf (slot-value message 'source) source)
  message)
